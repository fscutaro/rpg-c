#include <SDL.h>
#include <stdio.h>
#include "Game\Engine\Engine.h"
#include "Game\GameSource\States\ExploringMap.h"
#include "Game\GameSource\Constants.h"

int main( int argc, char* args[] )
{

	std::vector<State*> states;
	ExploringMap exploringMapState;

	states.push_back( &exploringMapState );

	int testGIT = 0;

	Engine gameEngine( "FRANO RPG", states, States::Names::EXPLORING_STATE, 1024, 768, 60 );

	return 0;
}