#pragma once
#include <SDL.h>
#include <iostream>
#include "Texture\TextureLoader.h"
#include <vector>
#include <list>

class Sprite
{
public:
	Sprite(std::string name);
	Sprite(std::string name,std::string path);
	Sprite(std::string name,SDL_Texture* texture);
	~Sprite();
	float x;
	float y;
	int width;
	int height;
	float rotation;
	SDL_Texture* getGraphics(bool skipComputeChildren);
	Sprite* getParent();
	void addChild(Sprite* sprite);
	void removeChild(Sprite* sprite);
	void setIsCameraTarget(bool value);
	bool getIsCameraTarget();
	std::string getName();
protected:
	SDL_Texture* _spriteGraphics;
	virtual void draw();
private:
	Sprite* _parent;
	std::list<Sprite*>* _children;
	std::string _name;
	void _initialize(std::string name);
	bool _isCameraTarget;
};

