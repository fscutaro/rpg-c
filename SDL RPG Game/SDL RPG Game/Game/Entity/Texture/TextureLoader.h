#pragma once
#include <SDL.h>
#include <iostream>

class TextureLoader
{
public:
	TextureLoader();
	~TextureLoader();
	SDL_Texture* loadTexture(std::string path);
	SDL_Surface* loadSurface(std::string path);
	SDL_Texture* getTextureFromSurfaceWithRect(SDL_Surface* surface, SDL_Rect* rect);
	SDL_Texture* getTextureFromSurfaceWithRect(SDL_Surface* surface, SDL_Rect* rect, bool useTransparent);
//private:
	//SDL_Surface* _tempSurface;
};

