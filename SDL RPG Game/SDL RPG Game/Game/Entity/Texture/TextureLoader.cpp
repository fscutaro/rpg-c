#include "TextureLoader.h"
#include <iostream>
#include "SDL_image.h"
#include "../../Rendering/Renderer.h"
#include <sdl.h>
TextureLoader::TextureLoader()
{
}

TextureLoader::~TextureLoader()
{
	//SDL_FreeSurface(_tempSurface);
	//_tempSurface = nullptr;
}

SDL_Texture* TextureLoader::loadTexture(std::string path)
{
	//The final texture
	SDL_Texture* newTexture = NULL;

	SDL_Surface* loadedSurface = loadSurface(path);
	if (loadedSurface == NULL)
	{
		printf("Unable to load image %s! SDL_image Error: %s\n", path.c_str(), IMG_GetError());
	}
	else
	{
		//Create texture from surface pixels
		newTexture = SDL_CreateTextureFromSurface(Renderer::getRenderer(), loadedSurface);
		if (newTexture == NULL)
		{
			printf("Unable to create texture from %s! SDL Error: %s\n", path.c_str(), SDL_GetError());
		}

		//Get rid of old loaded surface
		SDL_FreeSurface(loadedSurface);
	}

	return newTexture;
}

SDL_Surface* TextureLoader::loadSurface(std::string path)
{
	//Load image at specified path
	//SDL_Surface* loadedSurface = IMG_Load(path.c_str());
	//_tempSurface = SDL_CreateRGBSurface(0, loadedSurface->w, loadedSurface->h, 32, 0, 0, 0, 0);
	//return loadedSurface;
	return IMG_Load(path.c_str());
}

SDL_Texture* TextureLoader::getTextureFromSurfaceWithRect(SDL_Surface* surface, SDL_Rect* rect)
{
	return getTextureFromSurfaceWithRect(surface, rect, false);
}

SDL_Texture* TextureLoader::getTextureFromSurfaceWithRect(SDL_Surface* surface, SDL_Rect* rect, bool useTransparent)
{
	
	SDL_Surface* tempSurface = SDL_CreateRGBSurface(0, rect->w, rect->h, 32, 0, 0, 0, 0);
	SDL_BlitSurface(surface, rect, tempSurface, nullptr);
	if (useTransparent)
	{
		SDL_SetColorKey(tempSurface, SDL_TRUE, SDL_MapRGB(tempSurface->format, 0, 0, 0));
	}
	SDL_Texture* newTexture = SDL_CreateTextureFromSurface(Renderer::getRenderer(), tempSurface);
	SDL_FreeSurface(tempSurface);
	tempSurface = nullptr;
	return newTexture;
}
