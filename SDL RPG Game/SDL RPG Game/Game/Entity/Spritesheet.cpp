#include "Spritesheet.h"

Spritesheet::Spritesheet(std::string name, std::string path, std::vector<std::string>* animNames, int spriteWidth, int spriteHeight, int keyFramePerFrames) :Sprite(name)
{
	_animations = new std::map<std::string, std::vector<SDL_Texture*>*>();

	std::vector<SDL_Texture*>* textures;

	TextureLoader tl;
	SDL_Surface* surface = tl.loadSurface(path);

	int columns = surface->w / spriteWidth;
	int rows = surface->h / spriteHeight;

	int currentAnim = 0;
	int currentFrame = 0;

	width = spriteWidth;
	height = spriteHeight;

	_keyFramePerFrames = keyFramePerFrames;

	//TODO INVESTIGATE surface::clip_rect

	for (int i = 0; i < rows; i++)
	{
		textures = new std::vector<SDL_Texture*>();
		for (int j = 0; j < columns; j++)
		{
			SDL_Rect rect;

			rect.x = j * spriteWidth;
			rect.y = i * spriteHeight;

			rect.w = spriteWidth;
			rect.h = spriteHeight;

			textures->push_back(tl.getTextureFromSurfaceWithRect(surface, &rect, true));
			currentFrame++;
		}
		_animations->insert(std::make_pair(animNames->at(currentAnim), textures));
		currentFrame = 0;
		currentAnim++;
	}

	SDL_FreeSurface(surface);
	surface = nullptr;
	_currentAnimationName = animNames->at(0);
	_currentAnimation = _animations->at(_currentAnimationName);
	_spriteGraphics = _currentAnimation->at(_currentFrame);
}

Spritesheet::~Spritesheet()
{
	_animations->clear();
	_currentAnimation = nullptr;
	delete _animations;
}

void Spritesheet::draw()
{
	if (++_frameCounter >= _keyFramePerFrames)
	{
		_frameCounter = 0;
		_currentAnimation = _animations->at(_currentAnimationName);
		_spriteGraphics = _currentAnimation->at(_currentFrame);
		_currentFrame++;
		if (_currentFrame == _currentAnimation->size())
		{
			_currentFrame = 0;
		}
	}
}

void Spritesheet::setAnimation(const char* animName)
{
	if (strcmp(animName, _currentAnimationName.c_str()) != 0)
	{
		_currentAnimationName = animName;
		_frameCounter = 0;
		_currentFrame = 0;
	}

}
