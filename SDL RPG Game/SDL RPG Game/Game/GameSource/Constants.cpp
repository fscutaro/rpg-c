#pragma once
#include "Constants.h"
namespace States
{
	const char* Names::EXPLORING_STATE = "ExploringState";
}

namespace Animations
{
	const char* Names::UP    = "Up";
	const char* Names::DOWN  = "Down";
	const char* Names::LEFT  = "Left";
	const char* Names::RIGHT = "Right";
}