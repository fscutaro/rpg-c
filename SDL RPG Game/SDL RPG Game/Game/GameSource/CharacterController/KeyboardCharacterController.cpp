#include "KeyboardCharacterController.h"

KeyboardCharacterController::KeyboardCharacterController()
{
	_inputMan = new InputController(this);
	_inputMan->RegisterInput(SDL_SCANCODE_UP);
	_inputMan->RegisterInput(SDL_SCANCODE_DOWN);
	_inputMan->RegisterInput(SDL_SCANCODE_LEFT);
	_inputMan->RegisterInput(SDL_SCANCODE_RIGHT);
}


KeyboardCharacterController::~KeyboardCharacterController()
{
}

void KeyboardCharacterController::OnInputPressed(int inputCode)
{
	up    = inputCode == SDL_SCANCODE_UP;
	down  = inputCode == SDL_SCANCODE_DOWN;
	left  = inputCode == SDL_SCANCODE_LEFT;
	right = inputCode == SDL_SCANCODE_RIGHT;
}

void KeyboardCharacterController::OnInputReleased()
{
	up    = false;
	down  = false;
	left  = false;
	right = false;
}

void KeyboardCharacterController::update(float deltaTime)
{
	_inputMan->update();
}