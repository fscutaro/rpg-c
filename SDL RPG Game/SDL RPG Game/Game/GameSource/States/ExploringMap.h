#pragma once
#include "../../FSM/State.h"
#include "../../Entity/Sprite.h"
#include "../../Tilemap/TileMap.h"
#include "../Entities/Entity.h"
#include "../CharacterController/KeyboardCharacterController.h"

class ExploringMap :
	public State
{
public:
	ExploringMap();
	~ExploringMap();
	void enter();
	void sleep();
	void update(float deltaTime);
private:
	TileMap* _tileMap;
	Entity*  _hero;
	int _characterTileX;
	int _characterTileY;
	float _previousCharPosX;
	float _previousCharPosY;
};

