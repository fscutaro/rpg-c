#include "ExploringMap.h"
#include "../Constants.h"
#include <iostream>
#include <vector>
#include "../../Entity/Sprite.h"
#include "../../Engine/Debug.h"
#include "../../Entity/Spritesheet.h"
#include <string>
#include "../../Rendering/Camera.h"

ExploringMap::ExploringMap()
{
	_name = States::Names::EXPLORING_STATE;
}

void ExploringMap::enter()
{
	State::enter();

	if (Debug::isDebug())
	{
		std::cout << "ExploringMap::enter()" << std::endl;
	}
	_tileMap  = new TileMap("Assets/Maps/0.csv","Assets/Tiles/forest01.png");
	_hero     = new Entity("Hero", "Assets/Entities/nun2.png",16,18,5);
	_hero->setController(new KeyboardCharacterController());
	_hero->moveSpeed = 10;
	_hero->x = _tileMap->getCharPosX();
	_hero->y = _tileMap->getCharPosY();
	
	_screenGraphics->addChild(_tileMap);
	_screenGraphics->addChild(_hero   );

	Camera::setTarget(_hero);
}

void ExploringMap::update(float deltaTime)
{
	State::update(deltaTime);

	_previousCharPosX = _hero->x;
	_previousCharPosY = _hero->y;

	_hero->update(deltaTime);

	//TODO: Check if we should use float instead, there's a bug in which I can walk across mid tiles even if they aren't walkable.
	//We should check for more tiles when the number is not easily rounded ( 0.4 / 0.5 )
	_characterTileX = (int)(_hero->x / _tileMap->TILE_SIZE);
	_characterTileY = (int)(_hero->y / _tileMap->TILE_SIZE);

	//If the user is walking down, we should add one tile for collision checking purposes
	if (_hero->speedY > 0)
	{
		_characterTileY++;
	}

	//If the user is walking right, we should add one tile for collision checking purposes
	if (_hero->speedX > 0)
	{
		_characterTileX++;
	}

	if (_tileMap->checkCollision(_characterTileX, _characterTileY))
	{
		_hero->x = _previousCharPosX;
		_hero->y = _previousCharPosY;
	}

	if (Debug::isDebug())
	{
		//Commented. Too spammy
		//std::cout << "hero tile X Position " << _characterTileX << "hero tile Y Position " << _characterTileY << std::endl;
	}

}

void ExploringMap::sleep()
{
	std::vector<Sprite*>* mapSprites = _tileMap->getSprites();
	int mapSpritesLength = (*mapSprites).size();
	for (int i = 0; i < mapSpritesLength; i++)
	{
		_screenGraphics->removeChild((*mapSprites)[i]);
	}
	delete _tileMap;
	State::sleep();
}

ExploringMap::~ExploringMap()
{
	State::~State();
}
