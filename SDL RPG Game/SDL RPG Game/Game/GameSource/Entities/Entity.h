#pragma once
#include "../../Entity/Spritesheet.h"
#include "../CharacterController/CharacterController.h"
#include "../Constants.h"
class Entity :
	public Spritesheet
{
public:
	Entity(std::string name, std::string path, int spriteWidth, int spriteHeight, int keyFramePerFrames);
	~Entity();
	void setController(CharacterController* controller);
	void update(float deltaTime);
	float speedX;
	float speedY;
	float moveSpeed;
private:
	CharacterController* _controller;
};

