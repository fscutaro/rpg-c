#include "Entity.h"


Entity::Entity(std::string name, std::string path, int spriteWidth, int spriteHeight, int keyFramePerFrames):
	Spritesheet
	(
		name,
		path,
		new std::vector<std::string>
			{ 
				Animations::Names::UP,
				Animations::Names::RIGHT,
				Animations::Names::DOWN,
				Animations::Names::LEFT				
			},
		spriteWidth,
		spriteHeight,
		keyFramePerFrames
	)
{
}

Entity::~Entity()
{
	if (_controller != nullptr)
	{
		delete _controller;
		_controller = nullptr;
	}
}

void Entity::setController(CharacterController* controller)
{
	_controller = controller;
}

void Entity::update(float deltaTime)
{
	if (_controller != nullptr)
	{
		_controller->update(deltaTime);
		speedX = 0;
		speedY = 0;
		if (_controller->getUp())
		{
			speedY = -moveSpeed;
			setAnimation(Animations::Names::UP);
		}

		if (_controller->getDown())
		{
			speedY = moveSpeed;
			setAnimation(Animations::Names::DOWN);
		}

		if (_controller->getLeft())
		{
			speedX = -moveSpeed;
			setAnimation(Animations::Names::LEFT);
		}

		if (_controller->getRight())
		{
			speedX = moveSpeed;
			setAnimation(Animations::Names::RIGHT);
		}
	}

	x += speedX * deltaTime;
	y += speedY * deltaTime;
}
