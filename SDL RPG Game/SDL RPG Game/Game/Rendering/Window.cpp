#include "Window.h"

SDL_Window* Window ::_window	  = nullptr;
std::string* Window::_windowTitle = nullptr;
int Window		   ::_screenWidth = 0;
int Window		   ::_screenHeight= 0;

Window::Window()
{
}

Window::~Window()
{
	if (_window != nullptr)
	{
		SDL_DestroyWindow(_window);
		_window = nullptr;
	}
	delete _windowTitle;
	_windowTitle = nullptr;
}

SDL_Window* Window::createWindow(const char* title, int screenWidth, int screenHeight)
{
	_windowTitle  = new std::string( title );
	_screenWidth  = screenWidth;
	_screenHeight = screenHeight;
	return getWindow();
}

SDL_Window* Window::getWindow()
{
	if (_window == nullptr)
	{
		_window = SDL_CreateWindow(_windowTitle->c_str(), SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, _screenWidth, _screenHeight, SDL_WINDOW_SHOWN);
	}
	return _window;
}

int Window::getScreenWidth()
{
	return _screenWidth;
}

int Window::getScreenHeight()
{
	return _screenHeight;
}
