#include "Camera.h"
#include "../Rendering/Window.h"
Sprite* Camera::_target       = nullptr;
SDL_Rect* Camera::_targetRect = nullptr;

Camera::Camera()
{
}


Camera::~Camera()
{
	delete _targetRect;
	_target = nullptr;
}

//We set the sprite's parent as the target, and we assign the sprite bounds as target rect.
//This way we can render the parent. TODO:: check if there's a better way to do this. It won't work if we continue nesting sprites
void Camera::setTarget(Sprite* target)
{
	Sprite* targetParent = nullptr;
	if (_target != nullptr)
	{
		targetParent = _target->getParent();
		if (targetParent != nullptr)
		{
			targetParent->setIsCameraTarget(false);
		}	
	}
	_target = target;
	targetParent = _target->getParent();
	//Reset camera
	if (_target == nullptr)
	{
		_targetRect = nullptr;
	}
	else
	{
		if (targetParent != nullptr)
		{
			targetParent->setIsCameraTarget(true);
		}
	}
}

SDL_Rect* Camera::getTargetRect()
{
	if (_target != nullptr)
	{
		if (_targetRect == nullptr)
		{
			_targetRect = new SDL_Rect();
		}

		int zoom = 6;

		//TODO revisar esto. El zoom funciona mal
		float xCameraOffset = (_target->width / 2);
		float yCameraOffset = (_target->height / 2);
		/*_targetRect->x = ( _target->x - ( xCameraOffset * zoom ) ) ;
		_targetRect->y = ( _target->y - ( yCameraOffset * zoom ) ) ;
		_targetRect->w = _target->width  +(xCameraOffset *zoom);
		_targetRect->h = _target->height + (yCameraOffset *zoom);*/

		int screenWidth = Window::getScreenWidth();
		int screenHeight = Window::getScreenHeight();

		//Align on the horizontal axis
		if (screenWidth > screenHeight)
		{
			_targetRect->w = ( screenWidth / (screenHeight / _target->width) ) * zoom;
			_targetRect->h = _target->height * zoom;
		}
		else
		{
			_targetRect->w = _target->width * zoom ;
			_targetRect->h = ( screenHeight / (screenWidth / _target->width) ) * zoom;
		}

		float xDiff = _targetRect->w - _target->width;
		float yDiff = _targetRect->h - _target->height;
		_targetRect->x = _target->x - xDiff / 2;
		_targetRect->y = _target->y - yDiff / 2;

		//Keep the camera in bounds.
		if (_targetRect->x < 0)
		{
			_targetRect->x = 0;
		}
		if (_targetRect->y < 0)
		{
			_targetRect->y = 0;
		}
		if (_targetRect->x > screenWidth - _targetRect->w)
		{
			_targetRect->x = screenWidth - _targetRect->w;
		}
		if (_targetRect->y > screenHeight - _targetRect->h)
		{
			_targetRect->y = screenHeight - _targetRect->h;
		}
	}

	return _targetRect;
}
