#pragma once
#include <SDL.h>
#include <string>
class Window
{
public:
	Window();
	~Window();
	static SDL_Window* createWindow(const char* title, int screenWidth, int screenHeight);
	static SDL_Window* getWindow();
	static int getScreenWidth();
	static int getScreenHeight();
private:
	static SDL_Window* _window;
	static std::string* _windowTitle;
	static int _screenWidth;
	static int _screenHeight;
};

