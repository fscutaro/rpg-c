#include "Renderer.h"
#include "Window.h"
#include <iostream>
#include "../Entity/Sprite.h"
#include <vector>
#include "RenderList.h"
#include "SDL_image.h"
#include <SDL.h>
#include "../Engine/Engine.h"
#include "Camera.h"
SDL_Renderer* Renderer::_renderer = nullptr;

Renderer::Renderer()
{
	
}

Renderer::~Renderer()
{
	if (_renderer != nullptr)
	{
		SDL_DestroyRenderer(_renderer);
		_renderer = nullptr;
	}
}

SDL_Renderer* Renderer::getRenderer()
{
	if (_renderer == nullptr)
	{
		_renderer = SDL_CreateRenderer(Window::getWindow(), - 1, SDL_RENDERER_ACCELERATED);
		if (_renderer == NULL)
		{
			printf("Renderer could not be created! SDL Error: %s\n", SDL_GetError());
		}
		else
		{
			//Initialize renderer color
			SDL_SetRenderDrawColor(_renderer, 0xFF, 0xFF, 0xFF, 0xFF);

			//Initialize PNG loading
			int imgFlags = IMG_INIT_PNG;
			if (!(IMG_Init(imgFlags) & imgFlags))
			{
				printf("SDL_image could not initialize! SDL_image Error: %s\n", IMG_GetError());
			}
		}
	}

	return _renderer;
}

void Renderer::render()
{
	//Clear screen
	SDL_RenderClear(_renderer);

	SDL_RenderCopy(_renderer, Engine::getStage()->getGraphics(false), NULL, NULL);
	//Update screen

	SDL_RenderPresent(_renderer);
}
