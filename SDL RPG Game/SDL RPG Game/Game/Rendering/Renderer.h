#pragma once
#include <SDL.h>
class Renderer
{
public:
	Renderer();
	~Renderer();
	static SDL_Renderer* getRenderer();
	void render();
private:
	static SDL_Renderer* _renderer;
};

