#pragma once
#include "../External/rapidjson/document.h"
using namespace rapidjson;

class TileEDMapLoader
{
public:
	TileEDMapLoader(const char* path);
	~TileEDMapLoader();
	const Value& GetTilesArray();
private:
	Document* _doc;
};

