#pragma once
#include <string>
#include <stdlib.h>
#include <sstream>
#include <vector>
#include "../External/tinyxml2/tinyxml2.h"
using namespace tinyxml2;
using namespace std;
class TileEdXMLLoader
{
public:
	TileEdXMLLoader(std::string path);
	~TileEdXMLLoader();
	vector<vector<int>*>* getMapInfo();
	vector<int>* getWalkableTiles();
	int getCharXPos();
	int getCharYPos();
	string getTileSetPath();
private:
	void _processCSVTileMap(XMLNode* map);
	void _processObjects(XMLNode* map);
	void _processPatternFilePath(XMLNode* map);
	void _processTileSetAndWalkableTiles(XMLNode* tileSet);
	vector<int>* _walkableTiles;
	vector<vector<int>*>* _rows;
	string _patternFilePath;
	string _tileSetPath;
	int _charXPos;
	int _charYPos;
};

