#pragma once
#include <fstream>
#include <sstream>
#include <iostream>
#include <string>
#include <vector>

using namespace std;
class TileEDMapCSVLoader
{
public:
	TileEDMapCSVLoader(const char* path);
	~TileEDMapCSVLoader();
	vector<vector<int>*>* getMapInfo();
private:
	vector<vector<int>*>* _rows;
};

