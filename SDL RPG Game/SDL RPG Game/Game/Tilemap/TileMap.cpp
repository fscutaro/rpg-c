#include "TileMap.h"
#include <iostream>
#include "../Entity/Texture/TextureLoader.h"
#include <SDL.h>
#include "TileEDMapCSVLoader.h"
#include "TileEdXMLLoader.h"

/*
	HOW TO CREATE MAPS:
	- Maps are created using TileEd.
	- File type should be *.tmx (Using *.xml format)
	- Every *.tmx file should have a property "PatternFile" that points to the tilset (Tiles/somePattern.tsx)
	- Pattern files (*.tsx) should point to the *.png file used for the tileset. *.png and *.tsx should be in the same folder
	- Every tile in the pattern file should contain a property "Walkable"
*/

TileMap::TileMap(const char* path, const char* tilesPath):Sprite("TileMap")
{
	TileEdXMLLoader xmlLoader("Assets/Maps/0.tmx");

	_charPosX = xmlLoader.getCharXPos();
	_charPosY = xmlLoader.getCharYPos();

	TextureLoader tl;
	_tiles		   = tl.loadSurface(xmlLoader.getTileSetPath());
	_mapInfo	   = xmlLoader.getMapInfo();
	_walkableTiles = xmlLoader.getWalkableTiles();


	_tileSprites = new vector<Sprite*>();
	int rows = _mapInfo->size();
	int columns;
	int value;
	int mapWidthInTiles = _tiles->w / TILE_SIZE;
	const int EMPTY = -1;
	for (int i = 0; i < rows; i++)
	{
		columns = _mapInfo->at(i)->size();
		for (int j = 0; j < columns; j++)
		{
			value = _mapInfo->at(i)->at(j);
			if(value  == EMPTY)
			{
				continue;
			}

			SDL_Rect rect;
			//we need to transform Tile Ed values ( 1d array to 2d array https://softwareengineering.stackexchange.com/questions/212808/treating-a-1d-data-structure-as-2d-grid )

			rect.x = (value %  mapWidthInTiles) * mapWidthInTiles;
			rect.y = (value / mapWidthInTiles ) * mapWidthInTiles;

			rect.w = TILE_SIZE;
			rect.h = TILE_SIZE;

			Sprite* sprite = new Sprite("tile", tl.getTextureFromSurfaceWithRect(_tiles, &rect));
			sprite->x = j * TILE_SIZE;
			sprite->y = i *TILE_SIZE;
			_tileSprites->push_back(sprite);
			addChild(sprite);
		}
	}	
}

vector<Sprite*>* TileMap::getSprites()
{
	return _tileSprites;
}

int TileMap::getCharPosX()
{
	return _charPosX;
}

int TileMap::getCharPosY()
{
	return _charPosY;
}
//Returns "true" if the tile we're stepping on isn't walkable
bool TileMap::checkCollision(int tileX, int tileY)
{
	int tile = _mapInfo->at(tileY)->at(tileX);
	return !(std::find(_walkableTiles->begin(), _walkableTiles->end(), tile) != _walkableTiles->end());
}

TileMap::~TileMap()
{
	SDL_FreeSurface(_tiles);
	_tiles = nullptr;
	_tileSprites->clear();
	_tileSprites = nullptr;
	//delete _tiles;
	Sprite::~Sprite();
}
