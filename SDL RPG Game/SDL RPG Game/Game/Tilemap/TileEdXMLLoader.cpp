#include "TileEdXMLLoader.h"

//TODO: Error handling
TileEdXMLLoader::TileEdXMLLoader(std::string path)
{
	XMLDocument mapDoc;
	mapDoc.LoadFile(path.c_str());
	XMLNode* map = mapDoc.FirstChildElement("map");

	_processCSVTileMap(map);
	_processObjects(map);
	_processPatternFilePath(map);

	XMLDocument tileSetDoc;
	string tileSetPath = "Assets/" + _patternFilePath;
	tileSetDoc.LoadFile(tileSetPath.c_str());
	XMLNode* tileSet = tileSetDoc.FirstChildElement("tileset");
	_processTileSetAndWalkableTiles(tileSet);

	//TODO: Procesar Warps. Ver si puede ser todo en una sola funcion que incluya al main character
}


TileEdXMLLoader::~TileEdXMLLoader()
{
	_rows->clear();
	delete _rows;
	delete _walkableTiles;
}

vector<vector<int>*>* TileEdXMLLoader::getMapInfo()
{
	return new vector<vector<int>*>(_rows->begin(), _rows->end());
}


vector<int>* TileEdXMLLoader::getWalkableTiles()
{
	return new vector<int>(_walkableTiles->begin(), _walkableTiles->end());
}

string TileEdXMLLoader::getTileSetPath()
{
	return _tileSetPath;
}

int TileEdXMLLoader::getCharXPos()
{
	return _charXPos;
}

int TileEdXMLLoader::getCharYPos()
{
	return _charYPos;
}

void TileEdXMLLoader::_processCSVTileMap(XMLNode* map)
{
	XMLNode* layer = map->FirstChildElement("layer");
	XMLNode* data  = layer->FirstChildElement("data");
	//Get CSV map info
	const char* dataCSV = data->FirstChild()->Value(); //CSV map data

	string line; //Each line of CSV
	string item; //Each Tile
	stringstream str(dataCSV);
	int tileValue;
	_rows = new vector<vector<int>*>();
	while (getline(str, line, '\n'))
	{
		if (line.size() > 0)
		{
			//We push a new column
			vector<int>* columns = new vector<int>();
			_rows->push_back(columns);

			//we create a new string stream with our current line so we can split it with ","
			stringstream lineStream(line);

			while (getline(lineStream, item, ','))
			{
				tileValue = atoi(item.c_str()) - 1; //TMX files have an offset of 1 in the tile values
				columns->push_back(tileValue);
			}
		}
	}
}

void TileEdXMLLoader::_processObjects(XMLNode* map)
{
	XMLElement* entities = map->FirstChildElement("objectgroup");
	string objectGroupName = "";
	while (entities != nullptr)
	{
		objectGroupName = entities->Attribute("name");
		if ( !objectGroupName.compare( "Entities" ))
		{
			XMLElement* object = entities->FirstChildElement("object");
			string objectType = "";
			while (object != nullptr)
			{
				objectType = object->Attribute("type");
				if ( !objectType.compare( "MainCharacter") )
				{
					_charXPos = atoi(object->Attribute("x"));
					_charYPos = atoi(object->Attribute("y"));
					break;
				}
				object = object->NextSiblingElement();
			}
			break;
		}
		entities = entities->NextSiblingElement();
	}
}

void TileEdXMLLoader::_processPatternFilePath(XMLNode* map)
{
	XMLElement* properties = map->FirstChildElement("properties");
	XMLElement* property   = properties->FirstChildElement("property");
	string patternFileName = "";
	while (property != nullptr)
	{
		patternFileName = property->Attribute("name");
		if (!patternFileName.compare("PatternFile"))
		{
			_patternFilePath = property->Attribute("value");
			break;
		}
		property = property->NextSiblingElement();
	}
}

void TileEdXMLLoader::_processTileSetAndWalkableTiles(XMLNode* tileSet)
{
	XMLElement* image = tileSet->FirstChildElement("image");
	_tileSetPath = "Assets/Tiles/"+ (string)image->Attribute("source");

	XMLElement* tile = tileSet->FirstChildElement("tile");
	XMLElement* properties;
	XMLElement* property;
	string propertyName = "";
	string isWalkable;
	_walkableTiles = new vector<int>();
	while( tile != nullptr )
	{
		properties = tile->FirstChildElement("properties");
		property   = properties->FirstChildElement("property");
		while( property != nullptr )
		{
			propertyName = property->Attribute("name");
			if (!propertyName.compare("Walkable"))
			{
				isWalkable = property->Attribute("value");
				if (!isWalkable.compare("true"))
				{
					_walkableTiles->push_back( atoi(tile->Attribute("id")));
				}
			}
			property = property->NextSiblingElement();
		}
		tile = tile->NextSiblingElement("tile");
	}
}

