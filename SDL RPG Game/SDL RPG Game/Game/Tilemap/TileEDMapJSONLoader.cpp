#include "TileEDMapJSONLoader.h"
#include <fstream>
#include <iostream>
#include "../External/rapidjson/filereadstream.h"

using namespace rapidjson;
TileEDMapLoader::TileEDMapLoader(const char* path)
{
	FILE* fp;
	fopen_s(&fp, path, "rb");

	char readBuffer[65536];
	FileReadStream is(fp, readBuffer, sizeof(readBuffer));
	_doc = new Document();
	_doc->ParseStream(is);

	if ((*_doc)["layers"].IsArray())
	{
		std::cout << (*_doc)["layers"].IsString() << std::endl;
	}
	else
	{
		std::cout << "FAILED LOADING JSON " << path << std::endl;
	}	
}


TileEDMapLoader::~TileEDMapLoader()
{
	delete _doc;
}

//TODO:: return multiple layers
const Value& TileEDMapLoader::GetTilesArray()
{
	const Value& layers = (*_doc)["layers"];
	int length = layers.Size();
	for (int i = 0; i < length; i++)
	{
		const Value& value = layers[i];
		return value["data"];
	}

	std::cout << "TileEDMapLoader::GetTilesArray() ERROR - COULDN'T FIND ANY TILES";
}
