#pragma once
#include <SDL.h>
#include <vector>
#include "../Entity/Sprite.h"
using namespace std;
class TileMap:public Sprite
{
public:
	TileMap(const char* path, const char* tilesPath);
	~TileMap();
	static const int TILE_SIZE		  = 16;
	static const int TILE_MAP_COLUMNS = 50;
	static const int TILE_MAP_ROWS    = 38;
	vector<Sprite*>* getSprites();
	int getCharPosX();
	int getCharPosY();
	bool checkCollision(int tileX, int tileY);
private:
	SDL_Surface* _tiles;
	vector<Sprite*>* _tileSprites;
	vector<vector<int>*>* _mapInfo;
	vector<int>* _walkableTiles;
	int _charPosX;
	int _charPosY;
};

