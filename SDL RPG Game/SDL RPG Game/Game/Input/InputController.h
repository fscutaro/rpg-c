#pragma once
#include <vector>
#include "IInputHandler.h"
class InputController
{
public:
	InputController(IInputHandler* inputHandler);
	~InputController();
	void update();
	void RegisterInput(int scanCode);
private:
	std::vector<int>* _scanCodes;
	IInputHandler* _inputHandler;
};

