#pragma once
class IInputHandler
{
public:
	IInputHandler();
	~IInputHandler();
	virtual void OnInputPressed(int inputCode) = 0;
	virtual void OnInputReleased() = 0;
};

