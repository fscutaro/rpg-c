#pragma once
#include "State.h"
#include <iostream>
#include <vector>
class StateMachine
{
public:
	StateMachine(std::vector<State*> states, const char* initialState);
	~StateMachine();
	void update(float deltaTime);
	void switchState( const char* stateName );
private:
	State* _currentState;
	std::vector<State*> _states;
	State* _findState(const char* name);
};