#include "StateMachine.h"
#include "../Engine/Engine.h"

StateMachine::StateMachine(std::vector<State*> states, const char* initialState)
{
	_states = states;
	switchState(initialState);
}

StateMachine::~StateMachine()
{
	_states.clear();
	_currentState = nullptr;
}

void StateMachine::update( float deltaTime )
{
	if ( _currentState != nullptr )
	{
		_currentState->update( deltaTime );
	}
}

//Changes the current state for another one passed by parameter
void StateMachine::switchState(const char* stateName )
{
	if ( _currentState != nullptr )
	{
		if ( strcmp( _currentState->getName(), stateName ) == 0 )
		{
			//If the state we want to set is the current one, we don't do anything and just return
			return;
		}
		//Engine::getStage()->removeChild(_currentState);
		_currentState->sleep();
	}

	_currentState = _findState(stateName);
	//Engine::getStage()->addChild(_currentState);
	_currentState->enter();
}

State* StateMachine::_findState(const char* name)
{
	int count = _states.size();
	State* returnState = nullptr;
	for (int i = 0; i < count; i++)
	{
		returnState = _states.at(i);
		std::cout << strcmp(returnState->getName(), name) << std::endl;
		if (strcmp(returnState->getName(), name) == 0)
		{
			break;
		}
	}

	return returnState;
}