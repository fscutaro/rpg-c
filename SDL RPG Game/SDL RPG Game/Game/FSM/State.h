#pragma once
#include "../Entity/Sprite.h"
class State
{
public:
	State();
	~State();
	virtual void enter();
	virtual void update(float deltaTime);
	virtual void sleep();
	const char* getName() { return _name; };
protected:
	const char* _name;
	Sprite* _screenGraphics;
};

