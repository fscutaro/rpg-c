#include "State.h"
#include <iostream>
#include "../Engine/Debug.h"
#include "../Engine/Engine.h"

State::State()
{
}

State::~State()
{
}

void State::enter()
{
	if (Debug::isDebug())
	{
		std::cout << "State::enter()" << std::endl;
	}
		
	_screenGraphics = new Sprite( _name );
	Engine::getStage()->addChild(_screenGraphics);
}

void State::update(float deltaTime)
{
}

void State::sleep()
{
	Engine::getStage()->removeChild(_screenGraphics);
	delete _screenGraphics;
	_screenGraphics = nullptr;
}

