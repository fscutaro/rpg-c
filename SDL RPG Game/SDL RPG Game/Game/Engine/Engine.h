#pragma once
#include <iostream>
#include <vector>
#include "../FSM/State.h"
#include "../Entity/Sprite.h"
class Engine
{
public:
	Engine(const char* title, std::vector<State*> states,const char* initialState,int screenWidth, int screenHeight, int frameRate);
	~Engine();
	static Sprite* getStage();
private:
	static Sprite* _stage;
};

