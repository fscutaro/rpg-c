#include "Engine.h"
#include <stdio.h>
#include "../Rendering/Renderer.h"
#include "../Rendering/Window.h"
#include "../Entity/Sprite.h"
#include "../Rendering/RenderList.h"
#include "../FSM/StateMachine.h"
#include "Debug.h"

Sprite* Engine::_stage = nullptr;

Engine::Engine(const char* title, std::vector<State*> states, const char* initialState, int screenWidth, int screenHeight, int frameRate)
{
	//Window
	Window window;

	//Renderer class to draw 
	Renderer renderer;

	//True if the game is running. False otherwise
	bool isRunning = true;

	//delta time calculation
	float lastTime = 0;
	float currentTime = 0;
	float deltaTime;

	//fps calculation TODO CALCULATE FPS
	float fpsCounter = 0;
	float fpsTick = (1000 / (float)frameRate);



	//Initialize SDL
	if (SDL_Init(SDL_INIT_VIDEO) < 0)
	{
		printf("SDL could not initialize! SDL_Error: %s\n", SDL_GetError());
	}
	else
	{
		//We check if we can create a window and if so, we create it
		if (Window::createWindow(title, screenWidth, screenHeight) == NULL)
		{
			printf("Window could not be created! SDL_Error: %s\n", SDL_GetError());
		}
		else
		{
			/*
			We create our state machine that will manage all the screens in the game,
			passing as parameters all the states we got in the constructor and also
			the first state to be initialized
			*/
			StateMachine fsm(states, initialState);

			//We start the game loop
			while (isRunning)
			{
				//We listen for upcoming events
				SDL_Event e;
				while (SDL_PollEvent(&e))
				{
					if (e.type == SDL_QUIT)
					{
						isRunning = false;
						break;
					}
				}

				lastTime   = currentTime;
				fpsCounter = ( SDL_GetTicks() - lastTime );

				//Game loop will be limited to the frame rate
				if (fpsCounter > fpsTick)
				{
					fpsCounter  = 0;
					currentTime = SDL_GetTicks();
					deltaTime   = (currentTime - lastTime) / 100;

					//TODO: Listen for user inputs in here

					fsm.update(deltaTime);

					//Last step of our game loop: Render everything
					renderer.render();
				}
			}
		}
	}
}


Engine::~Engine()
{
	delete _stage;
	//Quit SDL subsystems
	SDL_Quit();
}

Sprite* Engine::getStage()
{
	if (_stage == nullptr)
	{
		_stage = new Sprite("Stage");
	}
	return _stage;
}
